## AZRS-TRACKING

##### Projekat u okviru kursa Alati za razvoj softvera na Matematičkom fakultetu. Služi za praćenje zadataka u okviru kojih se primenjuju alati. Zapravo u ovom projektu se vodi evidencija o progresu zadatka i opisuju se načini upotrebe alata koji su u tom zadatku korišćeni. Alati se primenjuju na projekat PATHFINDING ALGORITHMS VISUALIZATION koji je razvijan 2021/2022. godine u okviru kursa Razvoj softvera na Matematičkom fakultetu. Grupni projekat [PATHFINDING ALGORITHMS VISUALIZATION](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/17-pathfinding-algorithms-visualization) možete pogledati [ovde](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2021-2022/17-pathfinding-algorithms-visualization).

### Alati koji su primenjeni:
- Clang Tidy
- Clang Format
- Doxygen
- Git Hook

Nacin primene alata i spisak primenjenih alata mozete pogledati [ovde](https://gitlab.com/saraselakovic/azrs-tracking/-/issues/?sort=created_date&state=all&first_page_size=20).